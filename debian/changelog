tcpdf (6.8.2+dfsg-1) unstable; urgency=medium

  * New upstream version 6.8.2+dfsg
  * Switch to Git mode
  * Stop installing dev file tcpdf_import.php

 -- William Desportes <williamdes@wdes.fr>  Sun, 09 Feb 2025 17:41:13 +0100

tcpdf (6.8.0+dfsg-1) unstable; urgency=medium

  * New upstream version 6.8.0+dfsg
      - (CVE-2024-56519, CVE-2024-56520)
      - (CVE-2024-56521, CVE-2024-56522, CVE-2024-56527)
      - (Closes: #1091685, Closes: #1091686)
      - (Closes: #1091687, Closes: #1091688, Closes: #1091689)
  * Depend on php-curl
  * Recommend php-bcmath

 -- William Desportes <williamdes@wdes.fr>  Sat, 28 Dec 2024 11:01:35 +0100

tcpdf (6.7.7+dfsg-1) unstable; urgency=medium

  * New upstream version 6.7.7+dfsg
      - (CVE-2024-22641, CVE-2024-51058)
      - (Closes: #1072528, Closes: #1088332)
  * Bump Standards-Version to 4.7.0

 -- William Desportes <williamdes@wdes.fr>  Thu, 12 Dec 2024 09:25:25 +0100

tcpdf (6.7.5+dfsg-1) unstable; urgency=medium

  * New upstream version 6.7.5+dfsg (CVE-2024-22640, LP: #2062983)
  * Add CVE reference on 6.7.4+dfsg-1 changelog entry

 -- William Desportes <williamdes@wdes.fr>  Sat, 20 Apr 2024 20:49:38 +0200

tcpdf (6.7.4+dfsg-1) unstable; urgency=medium

  * New upstream version 6.7.4+dfsg (CVE-2024-32489, LP: 2062620)
  * Update example_066 patch

 -- William Desportes <williamdes@wdes.fr>  Sat, 06 Apr 2024 13:07:36 +0200

tcpdf (6.6.5+dfsg-1) unstable; urgency=medium

  * Change repacksuffix to dfsg
  * New upstream version 6.6.5+dfsg

 -- William Desportes <williamdes@wdes.fr>  Wed, 06 Sep 2023 19:51:29 +0200

tcpdf (6.6.3+dfsg1-1) unstable; urgency=medium

  * New upstream version 6.6.3+dfsg1
  * Add Changelog, Documentation, Security-Contact, CPE to d/u/metadata
  * Update d/copyright and update debian/*
  * Build the autoload from composer.json
  * Remove d/clean of include/sRGB.icc and use override_dh_install

 -- William Desportes <williamdes@wdes.fr>  Wed, 06 Sep 2023 14:53:58 +0200

tcpdf (6.6.2+dfsg1-1) unstable; urgency=medium

  * New upstream version 6.6.2+dfsg1
  * Refresh the example patch
  * Update Standards-Version: to 4.6.2

 -- William Desportes <williamdes@wdes.fr>  Sat, 24 Dec 2022 00:14:51 +0400

tcpdf (6.6.0+dfsg1-1) unstable; urgency=medium

  * New upstream version 6.6.0
  * Move the autoload test to a superficial DEP-8 test
  * Make autopkgtests depend on php-cli and not php

 -- William Desportes <williamdes@wdes.fr>  Tue, 06 Dec 2022 13:28:57 +0100

tcpdf (6.5.0+dfsg1-1) unstable; urgency=medium

  * New upstream version 6.5.0
  * Bump Standards-Version to 4.6.1
  * Run "wrap-and-sort"

 -- William Desportes <williamdes@wdes.fr>  Fri, 12 Aug 2022 10:18:42 +0200

tcpdf (6.4.4+dfsg1-1) unstable; urgency=medium

  * New upstream version 6.4.4 (Closes: #1000619)
  * Update tcpdf test.php from upstream
  * Update d/copyright
  * Add d/s/lintian-overrides
  * Update d/copyright years
  * Update test.sh to use the debian include path for tests
  * Depend on dh-sequence-phpcomposer and remove dh --with phpcomposer
  * Remove Felipe from Uploaders (no upload needed since 2020)
  * Install /u/s/p/autoloaders file
  * Use the debian autoload and not the class file directly in example 066

 -- William Desportes <williamdes@wdes.fr>  Wed, 05 Jan 2022 23:54:01 +0100

tcpdf (6.4.2+dfsg1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Wrap long lines in changelog entries: 6.3.5+dfsg1-1.

  [ William Desportes ]
  * New upstream version 6.4.2
  * Bump debhelper-compat to 13
  * Update Standards-Version to 4.6.0
  * Run "cme fix dpkg" to re-format the control file
  * Set debian branch to debian/latest (DEP-14)
  * Copy test script from upstream and update d/copyright for debian/*
  * Improve the autoload test
  * Run all examples like if they where tests
  * Add php-{gd,bcmath,json,xml}, poppler-utils as test dependencies
  * Add a patch to update the composer example number 66 to the Debian example

 -- William Desportes <williamdes@wdes.fr>  Thu, 19 Aug 2021 18:12:51 +0200

tcpdf (6.3.5+dfsg1-1) unstable; urgency=medium

  [ William Desportes ]
  * New upstream version 6.3.5.

  [ Felipe Sateler ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Add pkg-php-tools-override to enable automatic dependencies. Because the
    original composer name tecnickcom/tcpdf does not match the debian binary
    package name for historical reasons (php-tcpdf), we need to hint the correct
    name to dh_phpcomposer, so that reverse dependencies find the correct
    package automatically.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Felipe Sateler <fsateler@debian.org>  Sun, 15 Mar 2020 18:50:56 -0300

tcpdf (6.3.4+dfsg1-1) unstable; urgency=medium

  [ William Desportes ]
  * New upstream version 6.3.4.
  * Bump Standards-Version to 4.5.0
  * Update copyright year

 -- Felipe Sateler <fsateler@debian.org>  Sat, 15 Feb 2020 21:44:00 -0300

tcpdf (6.3.2+dfsg1-1) unstable; urgency=medium

  [ William Desportes ]
  * Take over package into the phpMyAdmin Team. (Closes: #889731)
    - Update Maintainer to "phpMyAdmin Team" and add Uploaders field
  * New upstream version 6.3.2 modified, fixes php errors and warnings.
    (Closes: #915286, LP: #1781000)
  * Add php autoloader. (Closes: #780039)
  * Fix VCS-urls and add GitLab CI file
  * Fix reprotest, add user_group www-data and use_sudo as required
  * Add debian/gbp.conf file and improve upstream import process
  * Upgrade standards from 4.1.3 to 4.4.1
  * Update copyright year and add Files-Excluded field

  [ Felipe Sateler ]
  * Change phpunit test to autoload test.
    There is no phpunit in tcpdf.
  * Make php-tcpdf depend on the icc profiles.
    This way, if there is ever an icc profile update we don't need to
    rebuild the package.
    As a bonus, we don't need to modify the source package during build

 -- Felipe Sateler <fsateler@debian.org>  Sun, 20 Oct 2019 11:48:54 -0300

tcpdf (6.2.26+dfsg-2) unstable; urgency=low

  [ Thiago Gomes Verissimo ]
  * QA upload.
  * Set Debian QA as maintainer.

  * Using new DH level format. Consequently:
    - debian/compat: removed.
    - debian/control: changed from 'debhelper' to 'debhelper-compat' in Build
      Depends field and bumped level to 12.
  * debian/control:
     - Bumped Standards-Version to 4.4.0.
  * debian/rules: enabled all hardening compilation flags.
  * debian/watch:
     - Fix Regex pattern to find new upstream code.
       package
  * debian/tests/*: created to provide simple CI test
  * debian/autoload.php.tpl:
    - Added a standard php autoload.php template to be used in CI tests

 -- Thiago Gomes Verissimo <verissimotgv@gmail.com>  Sun, 21 Jul 2019 22:23:26 -0300

tcpdf (6.2.26+dfsg-1) unstable; urgency=medium

  [ Emanuele Rocca ]
  * QA upload
  * New upstream release (Closes: #908866, CVE-2018-17057)

  [ Jelmer Vernooĳ ]
  * Use secure copyright file specification URI.
  * Trim trailing whitespace.

  [ Ondřej Nový ]
  * d/rules: Remove trailing whitespaces
  * d/watch: Use https protocol

 -- Emanuele Rocca <ema@debian.org>  Mon, 25 Feb 2019 22:23:26 +0100

tcpdf (6.2.13+dfsg-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.
  * Orphaning package as Laurent Destailleur was only maintaining this package
    as a dependency of dolibarr and he stopped maintaining the latter.
  * Move git repository to salsa.debian.org.
  * Switch to debhelper compat level 11.
  * Bump Standards-Version to 4.1.3.
  * Updated dependencies for PHP7.0. Closes: #817271 Thanks to Michal Čihař
    for the patch.
  * Update copyright file to avoid duplicate license entry on LGPL-3+
  * Improve get-orig-source.sh to replace sRGB.icc with its free variant.
  * Set "Rules-Requires-Root: binary-targets" because we need root rights to
    set ownership of /var/cache/tcpdf/ to www-data.

 -- Raphaël Hertzog <hertzog@debian.org>  Tue, 06 Feb 2018 15:42:26 +0100

tcpdf (6.2.12+dfsg2-1) unstable; urgency=medium

  * New upstream version 6.2.12 modified with free version of sRGB.icc.
    This solve lintian error.

 -- Laurent Destailleur (eldy) <eldy@users.sourceforge.net>  Sat, 27 Feb 2016 19:35:45 +0100

tcpdf (6.2.12+dfsg-1) unstable; urgency=medium

  * New upstream version 6.2.12 (Closes: #814030, #785212)
  * Update license files for qrcodes.php file (Closes: #780051)

 -- Laurent Destailleur (eldy) <eldy@users.sourceforge.net>  Tue, 23 Feb 2016 10:35:45 +0100

tcpdf (6.0.093+dfsg-1) unstable; urgency=medium

  * New upstream release 6.0.093+dfsg
  * Removed line "Files-Excluded: include/sRGB.icc" from debian/copyright.
    No more required since original tcpdf package contains free sRGB.icc.
  * Removed dependency on icc-profiles-free.
    No more required since original tcpdf package contains free sRGB.icc.
  * Removed link to sRGB.icc
    No more required since original tcpdf package contains free sRGB.icc.

 -- Laurent Destailleur (eldy) <eldy@users.sourceforge.net>  Sun, 07 Sep 2014 11:06:38 +0200

tcpdf (6.0.091+dfsg-1) unstable; urgency=low

  [ Laurent Destailleur (eldy) ]
  * New upstream release 6.0.091+dfsg
  * Fix: Removed file include/sRGB.icc. This remove support for PDF/A-1b but
    solve any license troubles (Closes: #757447)

  [ Raphaël Hertzog ]
  * Restore sRGB.icc as a symlink to icc-profiles-free's
    /usr/share/color/icc/sRGB.icc
  * Add “Files-Excluded: include/sRGB.icc” to debian/copyright so that
    uscan automatically repacks new upstream tarballs.

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 29 Aug 2014 22:06:12 +0200

tcpdf (6.0.083+dfsg-1) unstable; urgency=low

  * New upstream release version 6.0.083+dfsg

 -- Laurent Destailleur (eldy) <eldy@users.sourceforge.net>  Sat, 26 Jul 2014 13:07:54 +0200

tcpdf (6.0.048+dfsg-2) unstable; urgency=medium

  [ Laurent Destailleur (eldy) ]
  * Removed lintian warning composer-package-without-pkg-php-tools-builddep
    by removing the Build-Depends: pkg-php-tools that is not required.
  * Update Standards-Version to 3.9.5

 -- Raphaël Hertzog <hertzog@debian.org>  Sat, 04 Jan 2014 20:41:07 +0100

tcpdf (6.0.048+dfsg-1) unstable; urgency=low

  * Imported Upstream version 6.0.048+dfsg
  * Update copyright file for fonts.

 -- Laurent Destailleur (eldy) <eldy@users.sourceforge.net>  Mon, 25 Nov 2013 20:26:02 +0100

tcpdf (6.0.021+dfsg-1) unstable; urgency=low

  [ Laurent Destailleur (eldy) ]
  * Repackage the upstream sources from 6.0.021.

  [ Raphaël Hertzog ]
  * Drop cruft in debian/rules.
  * Drop ${phpcomposer:*} substvars from debian/control until we start using
    the phpcomposer dh addon.

 -- Laurent Destailleur (eldy) <eldy@users.sourceforge.net>  Wed, 31 Jul 2013 15:12:28 +0200

tcpdf (6.0.020+dfsg-1) unstable; urgency=low

  * Repackage the upstream sources from 6.0.020.
  * Removed directory images from php-tcpdf.install (does not exists anymore)

 -- Laurent Destailleur (eldy) <eldy@users.sourceforge.net>  Wed, 17 Jul 2013 20:58:25 +0200

tcpdf (6.0.010+dfsg-1) unstable; urgency=low

  * Repackage the upstream sources to drop fonts/free* which are copies of the
    GPL-licensed fonts provided by freefont but for which upstream doesn't
    provide the corresponding sources in their release tarball.
  * Update the copyright file to drop the paragraph about those files.
  * Provide debian/rules get-orig-source to repackage the upstream sources.

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 19 Apr 2013 17:52:58 +0200

tcpdf (6.0.010-1) unstable; urgency=low

  * Initial release. (Closes: #495985)
  * Install cache directory as a symlink to /var/cache/tcpdf (a www-data owned
    directory).
  * Add a README.Debian documenting some potential permissions issues.

 -- Raphaël Hertzog <hertzog@debian.org>  Thu, 18 Apr 2013 16:45:36 +0200
