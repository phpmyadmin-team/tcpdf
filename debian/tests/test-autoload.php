<?php

require '/usr/share/php/tcpdf/autoload.php';

$pdf = new TCPDF();

if ($pdf instanceof TCPDF) {
    fwrite(STDOUT, '[OK] instanceof matches' . PHP_EOL);
} else {
    fwrite(STDERR, '[FAIL] instanceof matches' . PHP_EOL);
    exit(1);
}

$expectedHeaderData = [
    'logo' => '',
    'logo_width' => 30,
    'title' => '',
    'string' => '',
    'text_color' =>
    [
        0,
        0,
        0,
    ],
    'line_color' =>
    [
        0,
        0,
        0,
    ]
];

if ($pdf->getHeaderData() === $expectedHeaderData) {
    fwrite(STDOUT, '[OK] Header data matches' . PHP_EOL);
    exit(0);
}

fwrite(STDERR, '[FAIL] Header data does not match test' . PHP_EOL);
exit(1);
